# Modern Login UI
#### March 8th 2020
#### By **peteHack**
![](screenshots/screenshot1.png)
![](screenshots/screenshot2.png)

## Description

```bash

An sample User Interface design with a modern feel.

```
## Technologies Used
[![forthebadge](https://forthebadge.com/images/badges/made-with-java.svg)](https://forthebadge.com)



## Installation
* `git clone https://gitlab.com/peter-wachira/modern-login.git` this repository
* `cd modern-login`

## Launching the Application

```bash
1.Launch the project in Android Studio

```


## Contact Details and Documentation

```bash

You can reach me via my personal email pwachira900@gmail.com

```



## License

- This project is licensed under the MIT Open Source license Copyright (c) 2019. [LICENCE](https://gitlab.com/peter-wachira/modern-login/-/blob/development/LICENSE)

[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/makes-people-smile.svg)](https://forthebadge.com)